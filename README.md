# M4UShopr #

A Flask-based web application for custom RepairShopr process flows.

## Setup ##

Install Python dependencies

```
pip install -r requirements.txt
```

Ensure [config.py](config.py) contains a valid API key with suitable permissions

## Running the Development Server ##

Set environment variables

```
export FLASK_APP=m4ushopr
export FLASK_ENV=development
```

Run development server

```
flask run --host=0.0.0.0
```
