import requests, json

class RepairShopr:
	def __init__(self, url, key):
		self.url = url
		self.headers = {'Authorization': f'Bearer {key}'}

	def request_wrapper(self, response):
		if response.status_code == 401:
			raise Exception('API Returned 401 - Invalid key or permissions')
		response.raise_for_status()
		return response

	def get(self, path, params={}):
		return self.request_wrapper(requests.get(f'{self.url}{path}', params=params, headers=self.headers))

	def put(self, path, data={}):
		return self.request_wrapper(requests.put(f'{self.url}{path}', data=data, headers=self.headers))

	def getTickets(self, page=1, number=None, status=None, query=''):
		params = {'page': page, 'number': number, 'status': status, 'query': query}
		response = self.get('/api/v1/tickets', params=params)
		data = json.loads(response.text)
		tickets = data['tickets']
		if page == 1:
			for nextPage in range(2, data['meta']['total_pages'] + 1):
				tickets += self.getTickets(page=nextPage, number=number, status=status, query=query)

		return tickets

	def getTicket(self, id):
		response = self.get(f'/api/v1/tickets/{id}')
		data = json.loads(response.text)
		data['ticket']['url'] = f'{self.url}/tickets/{data["ticket"]["id"]}'
		return data['ticket']

	def getFullTickets(self, tickets):
		return [self.getTicket(ticket['id']) for ticket in tickets]

	def setTicket(self, id, status=None, properties=None):
		fields = {}
		if status:
			fields['status'] = status
		if properties:
			fields['properties'] = properties
		response = self.put(f'/api/v1/tickets/{id}', data=fields)

	def ticketNumberToID(self, number):
		tickets = self.getTickets(number=number)
		return tickets[0]['id'] if len(tickets) > 0 else None
		

class TicketProcessor:
	@classmethod
	def process(cls, ticket, columns):
		ticket = TicketProcessor.injectAssets(ticket)
		ticket = TicketProcessor.injectProperties(ticket)
		processed = {}
		for column in columns:
			if column['field'] in ticket:
				processed[column['field']] = ticket[column['field']]
		return processed

	@classmethod
	def injectAssets(cls, ticket):
		if 'assets' in ticket:
			ticket['assets'] = ', '.join([f'{asset["name"]} - {asset["asset_serial"] if asset["asset_serial"] else "No Serial"}' for asset in ticket['assets']])
		return ticket
		
	@classmethod
	def injectProperties(cls, ticket):
		if 'properties' in ticket:
			for key, value in ticket['properties'].items():
				ticket[key] = value
		return ticket
