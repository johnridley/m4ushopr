from flask_wtf import FlaskForm
from wtforms import IntegerField, SubmitField
from wtforms.validators import DataRequired, NumberRange

class WarrantyRegister(FlaskForm):
    number = IntegerField('Ticket Number', validators=[DataRequired()])
