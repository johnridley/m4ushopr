from flask import render_template, redirect
import json

from m4ushopr import app
from .repairshopr import RepairShopr, TicketProcessor
from .forms import *

rs = RepairShopr(app.config['REPAIRSHOPR_URL'], app.config['REPAIRSHOPR_KEY'])

@app.route('/atwarranty')
def atwarranty():
    tickets = rs.getTickets(status='At Warranty Centre')
    columns = [{'title': 'Number', 'field': 'number', 'width': 100}, 
               {'title': 'Customer', 'field': 'customer_business_then_name'}, 
               {'title': 'Subject', 'field': 'subject'},
               {'title': 'URL', 'field': 'url', 'visible': False},
               {'title': 'Assets', 'field': 'assets', 'formatter': 'textarea'}]
    tickets = [TicketProcessor.process(ticket, columns) for ticket in rs.getFullTickets(tickets)]
    return render_template('table.html', title='At Warranty Centre', data=json.dumps(tickets), columns=json.dumps(columns))


@app.route('/setwarranty', methods=['GET', 'POST'])
def setwarranty():
    form = WarrantyRegister()
    if form.validate_on_submit():
        ticketId = rs.ticketNumberToID(int(form.number.data))
        rs.setTicket(ticketId, status='At Warranty Centre')
        return redirect('/atwarranty')
    return render_template('form.html', title='Set At Warranty', form=form)